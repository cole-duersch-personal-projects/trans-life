#Cole Duersch A02353428





import time
from random import randint
import sys
import os

# Variables
euphoriaLvl = 50
money = 100
clothes = []
actCount = 0
transAffirm = randint(1,3)
wearing = []
def mainMenu():
    time.sleep(3)
    global actCount
    if actCount >= 3:
        Night()
        pass
    answer = False
    while not answer:
        r = input("What would you like to do? [m]all, [f]amily, [g]ames, Friends[p], [s]tatus, [c]lothes: ")
        if (r!="m" and r!="f" and r!="g" and r!="p"and r!="q" and r!="s" and r!="c"):
            print ("Invalid response!")
    
        if r == "m":
            actCount +=1
            answer = True
            Mall()
        
        if r == "f":
            actCount+=1
            answer = True
            Family()
        
        if r == "g":
            actCount +=1
            answer = True
            Games()
        if r == "p":
            actCount+=1
            answer = True
            Friends()
        if r == "s":
            Status()
        if r == "q":
            print("Thanks for playing!")
            answer = True
            exit()
        if r == "c":
            Clothes()


def Night():
    clearScreen()
    global money
    print ("You had a wild day! Now it's time to sleep. Good night!")
  
    bonusChance = randint(1,10)
    if bonusChance == 1:
        print("You recieved a bonus of $30! That has been added to your paycheck!")
        money += 50
    else:
        print("Overnight your paycheck was deposited. You earned $20")
        money += 20
    time.sleep(5)
    Morning()

def endGame():
    clearScreen()
    if euphoriaLvl >= 100:
        print("You have achieved peak euphoria!\nI'm so happy for you sweetie!\nThanks for playing!")
        exit()
    if euphoriaLvl <= 0:
        print("You aren't getting the support you need. Every day is a slog to get through. You want to end it all and do sucessfully. I'm sorry it happened like this sweetie. Thank you for playing." )
        exit()
def Clothes():
    global clothes
    global euphoriaLvl
    clearScreen()
    print("Welcome to the clothes menu!")
    print("In just a second a list of clothes will print out.")
    print("Select the clothes you want to wear by typing in a number corresponding to how the list prints out")
    print("I.E.\nclothes1\nclothes2\nclothes3")
    print("")
    print("If you wanted to wear clothes1 you'd type 1. clothes2 would be 2 and so on")
    print("For each clothing item you have on you get a 3 point euphoria buff")
    print("Type 0 when you are done.")
    if len(clothes) == 0:
        print("You have no clothes to choose from")
        print("To purchase clothes visit the mall")
        mainMenu()
    else:
        done = False
        while not done:
            for item in clothes:
                print(item)
            wear = int(input("What would you like to wear? "))
            if wear != 0:
                wearing.append(clothes[(wear-1)]) 
                print("Clothing Equipped")
                clothes.pop((wear-1))
                print(wearing)
            if wear == 0:
                euphoriaLvl += 3*(len(wearing))
                done = True
        mainMenu()


def clearScreen():
    if sys.platform == "win":
        os.system("cls")
    if sys.platform == "darwin":
        os.system("clear")
    if sys.platform == "linux":
        os.system("clear")



def Status():
    print("Current Levels:\nEuphoria: ", euphoriaLvl)
    print("Money: $", money)
    mainMenu()

def Family():
    global transAffirm 
    global euphoriaLvl
    if transAffirm == 1:
        print("Your family is so happy to spend time with you!")
        activity = randint(1,5)
        if activity == 1:
            print("You decide to go to a movie and dinner.\nThey were both great and you were gendered correctly by the wait staff!")
            print("5 points have been added to your euphoria level")
            euphoriaLvl += 5
        if activity == 2:
            print("You attend your sibling's choir concert.\nYou were able to wear your favorite dress!")
            print("5 points have been added to your euphoria level")
            euphoriaLvl += 5
        if activity == 3:
            print("You go on a picnic!\nThe food was good and you got to wear a sundress!")
            print("5 points have been added to your euphoria level")
            euphoriaLvl += 5
        if activity == 4:
            print('You decide to go bowling!\nYou bowl a strike and your dad says, "Thats my girl!"')
            print("5 points have been added to your euphoria level")
            euphoriaLvl += 5
        if activity == 5:
            print("You have a Disney movie sing-along!\nYou're voice training has paid off! You were able to hit the alto and soprano parts!")
            print("5 points have been added to your euphoria level")
            euphoriaLvl += 5
    else:
        print("Your family is transphobic and doesn't want to spend time with you because of your identity.")
        print("5 points have been deducted from your euphoria level")
        euphoriaLvl -= 5
    
    mainMenu()

def Mall():
    global euphoriaLvl
    global clothes
    global money
    transphobia = randint(1,13)
    if transphobia < 14:
        print("You get clocked by a transphobe who calls you slurs")
        time.sleep(.5)
        print("You leave the mall with sad feelings.")
        print("Your euphoria level went down 5 points")
        euphoriaLvl -= 5
    else:
        bathChance = randint(1,5)
        if bathChance > 3 :
            print("You were able to use the bathroom in accordance with your gender identity!")
            print("Three points were added to your euphoria level")
            euphoriaLvl += 3
        else:
            pass
       
        items = ["White Mini[s]kirt $5", "Women's [J]eans $10", "Yellow [F]lannel $7", "[M]akeup $8", "Blue [B]louse $5","[T]ights $3"]
        print("You find a bunch of cute items!")
        for item in items:
            print(item)
        p = input("Choose one to buy! ")
        if p in ["s", "S"]:
            print("You bought the White Miniskirt!")
            clothes.append("White Miniskirt")
            money -=5

        if p in ["j", "J"]:
            print("You bought the Women's Jeans!")
            clothes.append("Jeans")
            money -= 10
        
        if p in ["f","F"]:
            print("You bought the Yellow Flannel!")
            clothes.append("Yellow Flannel")
            money -= 7

        if p in ["m", "M"]:
            print("You bought the Makeup!")
            clothes.append("Makeup")
            money -= 8
        if p in ["b", "B"]:
            print("You bought the Blue Blouse!")
            clothes.append("Blue Blouse")
            money -= 5
        if p in ["t", "T"]:
            print("You bought the tights!")
            clothes.append("Tights")
            money -= 3
        
        print("You are shopped out!")
    mainMenu()



def Games():
    global euphoriaLvl
    transphobia = randint(1,100)
    gameChoice = randint(1,2)
    if gameChoice == 1:
        print ("You decide to play Minecraft")
        time.sleep(1)
        if transphobia < 11:
            print("You're put in a server with transphobes who clock you from your username.")
            print("5 points have been deducted from your euphoria level")
            euphoriaLvl -= 5
        else:
            print("You are affirmed by those in your server and really like your character's skin.")
            print("5 points have been added to your euphoria level")
            euphoriaLvl += 5
    if gameChoice == 2:
        print("You decide to play Valorant")
        time.sleep(1)
        if transphobia < 11:
            print("You get clocked and called slurs in voice chat")
            print("5 points have been deducted from your euphoria level")
            euphoriaLvl -= 5
        else:
            print("You lock in Viper and get complimented on your voice!")
            print("5 points have been added to your euphoria level")
            euphoriaLvl += 5 
    mainMenu()

def Friends():
    global euphoriaLvl
    transphobia = randint(1,100)
    print("You hang out with your friends!")
    if transphobia > 6:
        print("One of your friends goes on a transphobic rant with you in the room")
        print("5 points have been deducted from your euphoria level")
        euphoriaLvl -= 5
    else:
        print("You have a great time!")
        print("They use your correct name and pronouns!")
        print("5 points have been added to your euphoria level")
        euphoriaLvl += 5
    time.sleep(2)
    mainMenu()




def Morning():
    global actCount
    actCount = 0
    global euphoriaLvl
    if euphoriaLvl >= 100 or euphoriaLvl <= 0:
        endGame()
    else:
        pass
        
    euphDysChance = randint(1,3)
    clearScreen()
    print("Good morning!")
    time.sleep(1)
    if money >= 500:
        print("You can afford bottom surgery!")
        time.sleep(1)
        surgChoice = input("Would you like to get the surgery done? y/n ")
        if surgChoice in ["y","Y"]:
            print("You get a vaginoplasty and are extremely happy with the results!")
            print("50 points have been added to your euphoria level")
            euphoriaLvl += 50

    if euphDysChance == 1:
        euphsentence = randint(1,4)
        if euphsentence == 1:
            print("As you go downstairs your mom uses your correct pronouns and compliments you properly!\n 5 points have been added to your euphoria level")
            euphoriaLvl += 5
        if euphsentence == 2:
            print("After you get dressed you look in the mirror and get really excited\n 5 points have been added to your euphoria level")
            euphoriaLvl += 5
        if euphsentence == 3:
            print("You hopped out of the shower and realized your breasts are growing\n 5 points have been added to your euphoria level")
            euphoriaLvl += 5
        if euphsentence == 4:
            print("The makeup you did today looks really good!\n 5 points have been added to your euphoria level")
            euphoriaLvl += 5
    else:
        dysphSentence = randint(1,4)
        if dysphSentence == 1:
            print("You go downstairs and your mom misgenders you\n5 points have been deducted from your euphoria level")
            euphoriaLvl -= 5
        if dysphSentence == 2:
            print("You pass by the mirror and notice your five o'clock shadow\n5 points have been deducted from your euphoria level")
            euphoriaLvl -= 5
        if dysphSentence == 3:
            print("Your body hair has been noticibly growing and you don't like it.\n5 points have been deducted from your euphoria level")
            euphoriaLvl -= 5
        if dysphSentence == 4: 
            print("Your voice is gravely and deep as you wake up and it makes you uncomfortable.\n5 points have been deducted from your euphoria level")
            euphoriaLvl -= 5
    print("You get up and take your hormones")
    time.sleep(1)
    mainMenu()



def Start():
    print("Welcome to the Trans Life Python Simulator")
    time.sleep(1)
    print("In this alpha version you will play through the life of a male to female transgender girl")
    time.sleep(1)
    print("Disclaimer: This game will deal with topics of transphobia and homophobia as well as suicide and mental illness. Player discretion is advised")
    time.sleep(1)

    answer = False
    while not answer:
        cont = input("Do you wish to continue? [y/n] ")
        if cont == "y":
            print("I hope you enjoy")
            Morning()
            answer = True
        elif cont == "n":
            print("Thanks for checking this out!\nHave a good one!")
            answer = True
            exit()

        if cont != "y" and cont != "n":
            print("Sorry that response was invalid. Please answer with y or n")



Start()

'''
Sources

Mall Transphobia Chance https://www.stonewall.org.uk/about-us/news/new-report-tells-us-how-public-actually-feel-about-trans-people

Accepting Family Chances https://www.pewresearch.org/social-trends/2022/06/28/americans-complex-views-on-gender-identity-and-transgender-issues/

Euphoria/Dysphoria Chances https://www.reddit.com/r/MtF/comments/z6mbt4/how_many_days_per_week_do_you_experience_euphoria/

'''