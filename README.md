# Trans Life

Thanks for checking out my Python-based text adventure called Trans Life!
In the current version you play as a trans girl trying to get through life.

## TRIGGER WARNING
This game deals with mental health issues and suicide. Player discretion is advised.

## Installing the game 

To install the game, you will first need Python which can be found here

https://www.python.org/downloads/

(If you are my Gender Psych teacher or TA skip this next step)

Run the installer and if the game isn't downloaded yet run this command

```
git clone https://gitlab.com/cole-duersch-personal-projects/trans-life.git

```



## Run the game
If you downloaded the game from my GitLab, run these commands
```
cd trans-life

python3 TransLife.py

```
If you got this from my Canvas zip file:

Install Python as mentioned above and then double click on "TransLife.py"